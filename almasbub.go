// Copyright © 2020 RCTIPlus <rctiplus.com>.
// almasbub package provides an easy and safe datatype cast in Go.
package almasbub

import (
	"time"
)

// ToBool return casts of an interface to a bool type.
// example:
// 		ToBool(1) // true
func ToBool(i interface{}) bool {
	v, _ := toBool(i)
	return v
}

// ToTime return casts of an interface to a time.Time type.
// example:
// 		ToTime(1) // time.Time{}
func ToTime(i interface{}) time.Time {
	v, _ := toTime(i)
	return v
}

// ToDuration return casts of an interface to a time.Duration type.
// example:
// 		ToDuration("1h") // time.Duration(1 * time.Hour)
func ToDuration(i interface{}) time.Duration {
	v, _ := toDuration(i)
	return v
}

// ToFloat64 return casts of an interface to a float64 type.
// example:
// 		ToFloat64(1) // 1.0 (float64)
func ToFloat64(i interface{}) float64 {
	v, _ := toFloat64(i)
	return v
}

// ToFloat32 return casts of an interface to a float32 type.
// example:
// 		ToFloat32(1.1) // 1.1 (float32)
func ToFloat32(i interface{}) float32 {
	v, _ := toFloat32(i)
	return v
}

// ToInt64 return casts of an interface to an int64 type.
// example:
// 		ToInt64(1) // 1 (int64)
func ToInt64(i interface{}) int64 {
	v, _ := toInt64(i)
	return v
}

// ToInt32 return casts of an interface to an int32 type.
// example:
// 		ToInt32(1) // 1 (int32)
func ToInt32(i interface{}) int32 {
	v, _ := toInt32(i)
	return v
}

// ToInt16 return casts of an interface to an int16 type.
// example:
// 		ToInt16(1) // 1 (int16)
func ToInt16(i interface{}) int16 {
	v, _ := toInt16(i)
	return v
}

// ToInt8 return casts of an interface to an int8 type.
// example:
// 		ToInt8(1) // 1 (int8)
func ToInt8(i interface{}) int8 {
	v, _ := toInt8(i)
	return v
}

// ToInt return casts of an interface to an int type.
// example:
// 		ToInt(1.1) // 1 (int)
func ToInt(i interface{}) int {
	v, _ := toInt(i)
	return v
}

// ToUint return casts of an interface to a uint type.
// example:
// 		ToUint(1.1) // 1 (uint)
func ToUint(i interface{}) uint {
	v, _ := toUint(i)
	return v
}

// ToUint64 return casts of an interface to a uint64 type.
// example:
// 		ToUint64(1) // 1 (uint64)
func ToUint64(i interface{}) uint64 {
	v, _ := toUint64(i)
	return v
}

// ToUint32 return casts of an interface to a uint32 type.
// example:
// 		ToUint32(1) // 1 (uint32)
func ToUint32(i interface{}) uint32 {
	v, _ := toUint32(i)
	return v
}

// ToUint16 return casts of an interface to a uint16 type.
// example:
// 		ToUint16(1) // 1 (uint16)
func ToUint16(i interface{}) uint16 {
	v, _ := toUint16(i)
	return v
}

// ToUint8 return casts of an interface to a uint8 type.
// example:
// 		ToUint8(1) // 1 (uint8)
func ToUint8(i interface{}) uint8 {
	v, _ := toUint8(i)
	return v
}

// ToString return casts of an interface to a string type.
// example:
// 		ToString(1) // "1" (string)
func ToString(i interface{}) string {
	v, _ := toString(i)
	return v
}

// ToStringDate return casts of an string time intto a time type and error if failed.
// example:
// 		ToStringDate("2020-01-01") // time.Time{2020-01-01 00:00:00 +0000 UTC}
func ToStringDate(i string) (time.Time, error) {
	v, err := stringToDate(i)
	return v, err
}