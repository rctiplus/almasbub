package almasbub

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

// Use ToBool function to cast anytype to bool type:
func TestToBool_should_true(t *testing.T) {
	v := ToBool(1)
	assert.Equal(t, true, v)
}

func TestToBool_should_false(t *testing.T) {
	v := ToBool(0)
	assert.Equal(t, false, v)
}

func TestToInt_should_1234(t *testing.T) {
	v := ToInt("1234")
	assert.Equal(t, 1234, v)
}

func TestToInt_should_0(t *testing.T) {
	v := ToInt("hello")
	assert.Equal(t, 0, v)
}

func TestToInt8_should_1234_int8(t *testing.T) {
	// range value of int bit is 0 - 127
	v := ToInt8(127)
	assert.Equal(t, int8(127), v)
}

func TestToInt8_should_0_int8(t *testing.T) {
	// range value of int bit is 0 - 127
	v := ToInt8("hello")
	assert.Equal(t, int8(0), v)
}

func TestToInt16_should_1234_int16(t *testing.T) {
	v := ToInt16("1234")
	assert.Equal(t, int16(1234), v)
}

func TestToInt16_should_0_int16(t *testing.T) {
	v := ToInt16("hello")
	assert.Equal(t, int16(0), v)
}

func TestToInt64_should_1234_int64(t *testing.T) {
	v := ToInt64("1234")
	assert.Equal(t, int64(1234), v)
}

func TestToInt64_should_0_int64(t *testing.T) {
	v := ToInt64("hello")
	assert.Equal(t, int64(0), v)
}

func TestToInt32_should_1234_int32(t *testing.T) {
	v := ToInt32("1234")
	assert.Equal(t, int32(1234), v)
}

func TestToInt32_should_0_int32(t *testing.T) {
	v := ToInt32("hello")
	assert.Equal(t, int32(0), v)
}

func TestToFloat32_should_1234_Float32(t *testing.T) {
	v := ToFloat32("1234")
	assert.Equal(t, float32(1234.0), v)
}

func TestToFloat32_should_0_Float32(t *testing.T) {
	v := ToFloat32("hello")
	assert.Equal(t, float32(0.0), v)
}

func TestToFloat64_should_1234_Float64(t *testing.T) {
	v := ToFloat64("1234")
	assert.Equal(t, float64(1234.0), v)
}

func TestToFloat64_should_0_Float64(t *testing.T) {
	v := ToFloat64("hello")
	assert.Equal(t, float64(0.0), v)
}

func TestToUint_should_1234_Uint(t *testing.T) {
	v := ToUint("1234")
	assert.Equal(t, uint(1234), v)
}

func TestToUint_should_0_Uint(t *testing.T) {
	v := ToUint("hello")
	assert.Equal(t, uint(0), v)
}

func TestToUint16_should_1234_Uint16(t *testing.T) {
	v := ToUint16("1234")
	assert.Equal(t, uint16(1234), v)
}

func TestToUint16_should_0_Uint16(t *testing.T) {
	v := ToUint16("hello")
	assert.Equal(t, uint16(0), v)
}

func TestToUint32_should_1234_Uint32(t *testing.T) {
	v := ToUint32("1234")
	assert.Equal(t, uint32(1234), v)
}

func TestToUint32_should_0_Uint32(t *testing.T) {
	v := ToUint32("hello")
	assert.Equal(t, uint32(0), v)
}

func TestToUint64_should_1234_Uint64(t *testing.T) {
	v := ToUint64("1234")
	assert.Equal(t, uint64(1234), v)
}

func TestToUint64_should_0_Uint64(t *testing.T) {
	v := ToUint64("hello")
	assert.Equal(t, uint64(0), v)
}

func TestToString_should_1234_string(t *testing.T) {
	v := ToString(1234)
	assert.Equal(t, "1234", v)
}

func TestToStringDate_should_2020_12_29_01_59_00_date(t *testing.T) {
	v, err := ToStringDate("2020-12-29 01:59:00")
	assert.NoError(t, err)
	assert.Equal(t, time.Time(time.Date(2020, time.December, 29, 1, 59, 0, 0, time.UTC)), v)
}
